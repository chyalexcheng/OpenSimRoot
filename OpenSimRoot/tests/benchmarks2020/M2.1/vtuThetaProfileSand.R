#!/usr/bin/env Rscript

#
#  script looks for a folder with vtu files and produces for each fem . vtu  
#  a summary of an data array.
#
#
#
#

args <- commandArgs(trailingOnly = TRUE)

#get script arguments
  getArgument<-function(argn,defaultsetting){
    n= 1+c(1:length(args))[args==argn]
    if(length(n)){
      args[n]
    }else{
      defaultsetting
    }
  }

#print help and exit
printhelp<-function(){
  cat(
    "\
R script for reducing 3d fem.vtu dat to 1 d arrays. Example usage:\
\
./vtuThetaProfile.R -o b3osr.xls swmsWithoutRoot/
\
additional arguments behind plotting command:\
  -h -help --help   this help\
  -o                outputfilename (default summarytable.xls)\
  -s                stepsize (default 0.1 cm)\
  -d                data array (default theta)\
\
"
  )
}
#print help
h=sum(args=="-h")+sum(args=="--help")+sum(args=="-help")
if(h | length(args)==0){
  printhelp()
  quit('no')
}


require(XML)

folder=args[length(args)]

filenames=dir(folder,pattern = "^fem.......vtu")
filenames=c("fem000.1000.vtu","fem000.2000.vtu","fem000.3000.vtu")
outputfilename=getArgument("-o","sand.xls")
stepSize=as.numeric(getArgument("-s","0.1"))
dataarrayname=getArgument("-d","theta")


getDepthProfile<-function(filename,dataarrayname="theta",FUN=mean,stepSize=1.){
  print(paste("processing",filename))
  ifile=xmlParse(filename)
  
  #header
  #<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">
  #<UnstructuredGrid>
  #  <Piece NumberOfPoints="5687" NumberOfCells="4600">
  #  <Points>
  #  <DataArray type="Float64" Name="Position" NumberOfComponents="3" format="ascii">
  
  xpCoords = xpathApply(ifile,"//VTKFile[@type='UnstructuredGrid']/UnstructuredGrid/Piece/Points/DataArray[@Name='Position']")
  if(xmlSize(xpCoords)!=1) stop("Did not find the Coordinates")
  
  
  #coords
  coordschar=unlist(strsplit(xmlValue(xpCoords[[1]]),split="[[:space:]]"))
  coordinates=as.numeric(coordschar)
  #double spaces lead to empty entries, which can be removed
  #coordschar[which(is.na(coordinates))]
  coordinates=na.exclude(coordinates)
  
  #declared length of array
  xpPiece = xpathApply(ifile,"//VTKFile[@type='UnstructuredGrid']/UnstructuredGrid/Piece")
  l=as.numeric(xmlAttrs(xpPiece[[1]])[["NumberOfPoints"]])
  #check that we found all
  if(l!=length(coordinates)/3.) stop("length of coordinates != declared length in piece")
  
  #array of interest
  datapath=paste("//VTKFile[@type='UnstructuredGrid']/UnstructuredGrid/Piece/PointData/DataArray[@Name='",dataarrayname,"']",sep="")
  xpData = xpathApply(ifile,datapath)
  if(xmlSize(xpData)!=1) stop(paste("Did not find the array name",dataarrayname))
  datachar=unlist(strsplit(xmlValue(xpData[[1]]),split="[[:space:]]"))
  datanum=as.numeric(datachar)
  datanum=na.exclude(datanum)
  if(l!=length(datanum)) stop("length of data != declared length in piece")
  
  #aggregate data along y coordinates
  #ycoordinates
  y=coordinates[((1:l)*3)-1]
  #round off on 1 cm values
  yfac=as.factor(stepSize*floor(y/stepSize))
  sumd=aggregate(datanum,by=list(depth=yfac),FUN)
  names(sumd)[2]=dataarrayname
  return(sumd)
}

d=getDepthProfile(paste(folder,filenames[1],sep="/"),dataarrayname = dataarrayname, stepSize = stepSize)
names(d)[2]=filenames[1]


for(filename in filenames[2:length(filenames)]){
  res=try(getDepthProfile(paste(folder,filename,sep="/"),dataarrayname = dataarrayname, stepSize = stepSize)[,2])
  if (class(res) != "try-error"){
    d[filename]=res
  }else{
    d[filename]=d[filename[2]]
  }  
}

#write.table(x = d, file = outputfilename, col.names = T, row.names = F)
#covert to meters
#levels(d[,1])<-as.numeric(levels(d[,1]))/100.

#select rows
#the timepoints are
# the water content is plotted for different times: 
# 0.1, 0.2, and 0.3 days for Scenario 1; 
# 0.2, 0.5, and 1 days for Scenario 2; 
# 0.1, 0.2, and 0.5 days for Scenario 3
#this is the 1(depth) 3, 4 ,5 and 7 and 12 th column? 
#and we have to repeat the depth column every time
d2=d[ , c(1,2,1,3,1,4)]
#flip rows and columns
d3=t(d2)
#write without headers
write.table(x = d3, file = outputfilename, col.names = F, row.names = F, sep=",", quote = F)

nd1=as.numeric(levels(d[,1]))[d[,1]] 
#nd1

#reference data
refd=read.csv(file="referenceSolution.txt",header=F) #dim is 5,200
refdt=t(refd[1:6,])#1:6 is sand, 7:12 is loam. 13:18 is clay

svg("thetaByDepthSand.svg")
plot(d[,2],nd1,type='l',col=1,xlim=range(d[,2:4],refdt[,c(2,4,6)]),ylab="depth (cm)",xlab="theta")
lines(d[,3],nd1,col=2)
lines(d[,4],nd1,col=3)
#lines(d[,7],nd1,col=4)
#lines(d[,12],nd1,col=5)

#ref
points(refdt[,2],refdt[,1],col=1)
points(refdt[,4],refdt[,3],col=2)
points(refdt[,6],refdt[,5],col=3)



legend("bottomright",legend = c("0.1","0.2","0.3","0.5","1."),col = 1:5, lty=1)

dev.off()


