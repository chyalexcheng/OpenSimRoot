#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from scipy import optimize
from scipy import integrate
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
import os
import ast
from van_genuchten import *

sand = Parameters(0.045, 0.43, 0.15, 3, 1000,0.5) 
loam = Parameters(0.08, 0.43, 0.04, 1.6, 50,0.5) 
clay = Parameters(0.1, 0.4, 0.01, 1.1, 10,0.5) 
r_root = 0.02 # cm
r_out = 0.6 # cm
rho = r_out/r_root # 1

def plot_analytical():
    global h_stressonset, wc_stressonset
    fig, ax = plt.subplots(2, 3,figsize=(14, 14))
    h_out = np.linspace(-100,-14500,100) #cm
    h_lim = -15000 # cm
    q_out = 0 # q_root * (r_root/(2*r_out))
    r = np.linspace(r_root, r_out, 30)
    h_stressonset = np.zeros((6,len(r))) 
    wc_stressonset = np.zeros((6,len(r)))
    ind = 0 
    soils = ["Sand", "Loam", "Clay"]
    for s,soil in enumerate([sand, loam, clay]): 
        for q,q_root in enumerate([0.1,0.05]):    # q_root in [cm/d], high and low transpiration
            crit = 0 
            k = 0
            while crit < 1:
                MFP_nostress = MFP(h_out[k],soil) + (q_root*r_root-q_out*r_out)*(r**2/r_root**2/(2*(1-rho**2))+rho**2/(1-rho**2)*(np.log(r_out/r)-0.5)) + q_out*r_out*np.log(r/r_out)
                if np.amin(MFP_nostress)<=0: # stress   
                    MFP_stress  = (MFP(h_out[k],soil)+q_out*r_out*np.log(1/rho))*( (r**2/r_root**2 - 1 + 2*rho**2*np.log(r_root/r))/(rho**2 - 1+2*rho**2*np.log(1/rho)) ) + q_out*r_out*np.log(r/r_root)
                    for i in range(len(r)):        
                        h_stressonset[ind,i] = h(MFP_stress[i],soil) 
                        wc_stressonset[ind,i] = water_content(h_stressonset[ind,i],soil)
                    crit=crit+1
                k=k+1

            Q = np.trapz(wc_stressonset[ind,:]*r,r)
            simtime=((r_out**2-r_root**2)*3.14*water_content(-100,soil) - Q*2*3.14)/(2*r_root*3.14*q_root)
            ax[int(q),int(s)].plot(r,h_stressonset[ind,:],'r*',label='Ref, '+"{:.1f}".format(simtime)+" day")
            ax[1,s].set_xlabel("distance to axis (cm)")
            ax[q,0].set_ylabel("soil water pressure head (cm)")
            ax[q,1].set_yticklabels([]);ax[int(q),2].set_yticklabels([])
            ax[q,s].set_ylim(-15000,0)
            ax[q,s].legend() 
            ax[q,s].title.set_text(soils[int(s)]+r', q$_{r}$='+"{:.2f}".format(q_root)+" cm/day")
            ind=ind+1
    return fig, ax
# fig, ax = plot_analytical()
# plt.show()

fig, ax = plot_analytical() # prepare analytic solution plot
r, nrmse, nn, l, lsoil, lqroot = [], [], [], [], [], []
lsoils=["Sand","Loam","Clay"] 
lqroots=[0.1,0.05]
stressonset={'dumux-rosi':[0.0, 10.0, 8.34,0, 20.92, 17.26],
             'OpenSimRoot':[0,11.14,9.24,0.,23.14,18.84]}

cc=0;
for dirname, dirnames, filenames in os.walk('results/.'):    
    filenames.sort(key=str.lower)
    for i,f in enumerate(filenames):            
        try: 
            data = []
            with open('results/'+f) as fp: 
                for line in fp:
                    s = ast.literal_eval("["+line+"]")                
                    data.append(s)                
            if "_ks" in f: 
                cc -= 1; ls="dashed"
            else:
                ls="solid"                
            stressonset_=stressonset.get(f)
            for k in range(0,3): # axis
                for j in range(0,2): 
                    if stressonset_ is None:
                        stresslabel="-"
                    else:
                        stresslabel=str(round(stressonset_[3*j+k],2))
                    r_n = data[2*(3*j+k)]
                    h_n = data[2*(3*j+k)+1]        
                    ax[j,k].plot(r_n, h_n, color = col[cc], label = f+" "+stresslabel+" days", linewidth = 1.5, linestyle=ls) # numeric solution 
                    ax[j,k].legend(prop={'size': 12}) # update legend
                    l.append(f) 
                    lsoil.append(lsoils[k]) 
                    lqroot.append(lqroots[j])             
                    nn.append(len(h_n))                     
                    r_a = np.linspace(r_root, r_out, 30)
                    h_a = h_stressonset[3*j+k,:]
                    interp = interp1d(r_n, h_n, kind='linear',fill_value='extrapolate', bounds_error = False)
                    h_n = interp(sorted(r_a)) # numerical solution at points of analytical solution                                     
                    nrmse.append(nRMSE(h_a,h_n)) # calculate metrics    
            cc += 1
        except Exception as ex:
            print("Something went wrong with file "+f)    
            raise                  

plt.savefig('c11_results.png', dpi=300, bbox_inches = "tight") 
#plt.show()
