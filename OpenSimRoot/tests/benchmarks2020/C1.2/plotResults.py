#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate
from scipy.interpolate import interp1d
import os
from van_genuchten import *

def sinusoidal(t): # to calculate potential transpiration
    return np.sin(2. * np.pi * np.array(t) - 0.5 * np.pi) + 1.

def plot_all(path):
    nrmse, names = [], []
    fig, ax = plt.subplots(2, 1, figsize=(14, 18))
    data = np.loadtxt(path +"reference", delimiter=';')  
    t_a=data[0,:] 
    y_a=data[1,:] 
    cumy_a = integrate.cumtrapz(data[1,:],data[0,:],initial=0)
    ax[0].plot(t_a, 6.4*sinusoidal(t_a), 'k', label = "potential transpiration")
    ax[0].plot(t_a,y_a,'r*', label = "DuMu$^x$ explicit interface")
    ax[0].set_xlabel("time (day)")
    ax[0].set_ylabel("transpiration (cm$^3$ day$^{-1}$)")
    ax[1].plot(t_a,integrate.cumtrapz(y_a,t_a,initial=0),'r*', label = "DuMu$^x$ explicit interface")
    ax[1].set_xlabel("time (day)")
    ax[1].set_ylabel("cumulative uptake (cm$^3$)")
    cc = 0
    for dirname, dirnames, filenames in os.walk(path+'.'):
        filenames.sort(key=str.lower)
        for i,f in enumerate(filenames):
            try:
                if f != "reference":                    
                    data = np.loadtxt(path+f,delimiter=';')  
                    t_ = data[0,data[0,:]<=3] 
                    a_ = data[1,data[0,:]<=3]
                    if f.endswith("_rhizo"): # rhizosphere model
                        cc -= 1 # last color
                        ax[0].plot(t_,a_, color = col[cc], label = f, linewidth = 1.5, alpha = 1, linestyle = "--")
                        ax[1].plot(t_,integrate.cumtrapz(a_,t_,initial=0), color = col[cc], label = f, linewidth = 1.5, alpha = 1, linestyle = "--")                  
                    elif f.endswith("_ks"): # DuMux rhizosphere model with kernel support
                        cc -= 1 # last color
                        ax[0].plot(t_,a_, color = col[cc], label = f, linewidth = 1.5, alpha = 1, linestyle = "-.")
                        ax[1].plot(t_,integrate.cumtrapz(a_,t_,initial=0), color = col[cc], label = f, linewidth = 1.5, alpha = 1, linestyle = "-.")                                            
                    else: # other sink term
                        ax[0].plot(t_,a_, color = col[cc], label = f, linewidth = 1.5, alpha = 1)                        
                        ax[1].plot(t_,integrate.cumtrapz(a_,t_,initial=0), color = col[cc], label = f, linewidth = 1.5, alpha = 1)                  
                    cc += 1
                    # for table
                    interp = interp1d(t_, integrate.cumtrapz(a_,t_,initial=0),  kind='linear', fill_value='extrapolate', bounds_error =False ) 
                    cumy_n = interp(t_a) 
                    nrmse.append(nRMSE(cumy_a, cumy_n))
                    names.append(f)
            except Exception as ex:
                print("Something went wrong with file "+f)    
                raise
    ax[0].legend(loc = 'upper right')
    ax[1].legend(loc = 'upper left')
    ax[1].set_ylim(0,15) # fix y axis
    return nrmse, names      


nrmsea, namea = plot_all("results_a/")
plt.savefig('c12a_results.png', dpi=300, bbox_inches = "tight") 

nrmseb, nameb = plot_all("results_b/")
plt.savefig('c12b_results.png', dpi=300, bbox_inches = "tight") 

