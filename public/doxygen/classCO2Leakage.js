var classCO2Leakage =
[
    [ "CO2Leakage", "classCO2Leakage.html#a9cef2f1ca83a1c05d01b4d52ac61305d", null ],
    [ "calculate", "classCO2Leakage.html#ac86cf388bef2aede6caeb9f2376251cb", null ],
    [ "getName", "classCO2Leakage.html#a11aa559619ddb5e31e583af6a7f9e710", null ],
    [ "pLeafTemperature", "classCO2Leakage.html#a8520eb4024edd1fa1a31b7e9223bba3e", null ],
    [ "pMesophyllC", "classCO2Leakage.html#ad62ee2fcd656ae442e6384b5310e2e75", null ],
    [ "pSheathC", "classCO2Leakage.html#a7353dba85eb496389a6ec77594570151", null ],
    [ "sheathConductanceActivationEnergy", "classCO2Leakage.html#ac67dd2b65ae08baa22100a5473dd3c61", null ],
    [ "sheathConductanceAt25C", "classCO2Leakage.html#a318707a72a024f3d1b0ee1b803c1b217", null ],
    [ "sheathConductanceDeactivationEnergy", "classCO2Leakage.html#abd023e96dc2c92ed43dbbdafb931888a", null ],
    [ "sheathConductanceEntropyTerm", "classCO2Leakage.html#a59838a512105813901314332c3e5f010", null ]
];