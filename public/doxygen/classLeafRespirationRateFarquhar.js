var classLeafRespirationRateFarquhar =
[
    [ "LeafRespirationRateFarquhar", "classLeafRespirationRateFarquhar.html#a6b2af553bd60749eeacb01afa7903927", null ],
    [ "calculate", "classLeafRespirationRateFarquhar.html#a05def22cb780eb63811d5f0b637b71e1", null ],
    [ "getName", "classLeafRespirationRateFarquhar.html#a00509358b23fd584a82cb1eaaa9b809b", null ],
    [ "activationEnergyDayRespiration", "classLeafRespirationRateFarquhar.html#a7d214ac118e56c453f6c8307ef183b3f", null ],
    [ "cachedLeafTemperature", "classLeafRespirationRateFarquhar.html#a4e02c0ae880bf50fa19eb93c610fe928", null ],
    [ "cachedTime", "classLeafRespirationRateFarquhar.html#ab871e12f4f30302c7a1933406b777e9e", null ],
    [ "leafArea", "classLeafRespirationRateFarquhar.html#a655e3f0e8184b14a5094addb4a85f621", null ],
    [ "pLeafArea", "classLeafRespirationRateFarquhar.html#aeb55aaad9f62b40fdd96267b50eb974a", null ],
    [ "pLeafDryWeight", "classLeafRespirationRateFarquhar.html#afeef021b7b0bf3e9d85c6dae0d1c7c0b", null ],
    [ "pLeafSenescence", "classLeafRespirationRateFarquhar.html#a43f3a0da50ad9b70bf6fb145acb21391", null ],
    [ "pLeafTemperature", "classLeafRespirationRateFarquhar.html#aac7ddcd6a220841a597356da936828f1", null ],
    [ "pReferenceDayRespiration", "classLeafRespirationRateFarquhar.html#a3179003f0fe68a957959dda9933a9ae5", null ],
    [ "pStemDryWeight", "classLeafRespirationRateFarquhar.html#a21508133f75a87c7ebe50f32b1688557", null ],
    [ "referenceDayRespiration", "classLeafRespirationRateFarquhar.html#a71ca89afa0fa4518eefff919ba2ad11b", null ],
    [ "temperatureScalingFactor", "classLeafRespirationRateFarquhar.html#a2dcf35bb909f519cd5d6092d976f4403", null ]
];