var dir_feda37c8d9dade464702742535aa61d2 =
[
    [ "Coordinates.cpp", "Coordinates_8cpp.html", "Coordinates_8cpp" ],
    [ "Coordinates.hpp", "Coordinates_8hpp.html", "Coordinates_8hpp" ],
    [ "CustomMap.cpp", "CustomMap_8cpp.html", null ],
    [ "CustomMap.hpp", "CustomMap_8hpp.html", [
      [ "osrMapLess", "structosrMapLess.html", "structosrMapLess" ],
      [ "osrMap", "classosrMap.html", "classosrMap" ]
    ] ],
    [ "ReferenceLists.cpp", "ReferenceLists_8cpp.html", null ],
    [ "ReferenceLists.hpp", "ReferenceLists_8hpp.html", [
      [ "LessStringPointerRefList", "classLessStringPointerRefList.html", "classLessStringPointerRefList" ],
      [ "ReferenceList", "classReferenceList.html", "classReferenceList" ]
    ] ],
    [ "StateRate.cpp", "StateRate_8cpp.html", "StateRate_8cpp" ],
    [ "StateRate.hpp", "StateRate_8hpp.html", "StateRate_8hpp" ],
    [ "TagClass.cpp", "TagClass_8cpp.html", "TagClass_8cpp" ],
    [ "TagClass.hpp", "TagClass_8hpp.html", "TagClass_8hpp" ],
    [ "Time.hpp", "engine_2DataDefinitions_2Time_8hpp.html", "engine_2DataDefinitions_2Time_8hpp" ],
    [ "Units.cpp", "Units_8cpp.html", "Units_8cpp" ],
    [ "Units.hpp", "Units_8hpp.html", "Units_8hpp" ]
];