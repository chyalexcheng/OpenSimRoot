var classUnit =
[
    [ "Unit", "classUnit.html#a8e46f663a95736c8002d85ab271a7581", null ],
    [ "Unit", "classUnit.html#a71f3d8acd80f5dd938bfdf50879afe21", null ],
    [ "Unit", "classUnit.html#ab56f99e51a78061dc21e32fd23cf5ba8", null ],
    [ "Unit", "classUnit.html#aeaf5dcab742fadd858e982352d28051e", null ],
    [ "Unit", "classUnit.html#af283cb152cf159329fc2b4436e632c34", null ],
    [ "check", "classUnit.html#a3fa64453fbc47a06fd36a7583a65e27b", null ],
    [ "element", "classUnit.html#a10b224d05cf6a889f890e21267edcb52", null ],
    [ "formattedName", "classUnit.html#adea20171ebcfa3fac792e76231bb7e43", null ],
    [ "getUnitConversionFactor", "classUnit.html#afaa79694d4d0df679673052608af94b8", null ],
    [ "operator!=", "classUnit.html#a34c5711f5d2cb8910ae6263cc9e059ef", null ],
    [ "operator*", "classUnit.html#a0208b1910c8d16b1055be3f4d7f1bd09", null ],
    [ "operator*=", "classUnit.html#aadc95c38d11e2fab992510c113bb9d7f", null ],
    [ "operator/", "classUnit.html#a45c0c5041e03cd37a820a4160ee6a870", null ],
    [ "operator/=", "classUnit.html#a41461a32a562a7eaef96feee3cbd8689", null ],
    [ "operator<", "classUnit.html#a830fbaaf1734d926acf4c241098dd971", null ],
    [ "operator=", "classUnit.html#ae64cf47bc8cc4dc200c70bd2c921b98a", null ],
    [ "operator==", "classUnit.html#a31f216d06e1cc1c6134290387cf62811", null ],
    [ "operator>", "classUnit.html#a30340ca53eeb67a5221a4f7336ceee24", null ],
    [ "factor", "classUnit.html#a85c1c123ddd4879938a644c1cef016c5", null ],
    [ "name", "classUnit.html#a03a2344fa6977a2ce44b320e44bc9caa", null ],
    [ "order", "classUnit.html#a2c671f132a2d705f01c42fd143381b3d", null ]
];