var classPhotosynthesisFarquhar =
[
    [ "PhotosynthesisFarquhar", "classPhotosynthesisFarquhar.html#a9d21db0af9f9b39912d68b89b012d46d", null ],
    [ "calculate", "classPhotosynthesisFarquhar.html#ac9afcc9a3e445957176606d879bc7bf2", null ],
    [ "getName", "classPhotosynthesisFarquhar.html#ab4431f3f265c953e0daa988d8c76c825", null ],
    [ "pLeafArea", "classPhotosynthesisFarquhar.html#a6944a248bae7d0525cc5168b87128f3d", null ],
    [ "pPhotoPeriod", "classPhotosynthesisFarquhar.html#ae7d08d9f38881052dd94f3e0dd84f528", null ],
    [ "pPhotosynthesisC", "classPhotosynthesisFarquhar.html#ad3649aae1be0d77c30a9b9e89d777deb", null ],
    [ "pPhotosynthesisJ", "classPhotosynthesisFarquhar.html#a44348ba2d66e3595f0ce024ba69b06e8", null ],
    [ "pPhotosynthesisP", "classPhotosynthesisFarquhar.html#a707ffe18b98b056a73a28d5054649a7c", null ]
];