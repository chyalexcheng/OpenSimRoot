var classRootImpedanceWhalley =
[
    [ "RootImpedanceWhalley", "classRootImpedanceWhalley.html#a48c014aa89b1ca2f6eefc657de814963", null ],
    [ "calculate", "classRootImpedanceWhalley.html#a706b351bb90f571b9b35ac97454f81ef", null ],
    [ "getName", "classRootImpedanceWhalley.html#a8de58d566f8c6e296431970ff8b7e7a8", null ],
    [ "bulkDensityFactor", "classRootImpedanceWhalley.html#a23225d4f6f325090e23989e338fc7ebb", null ],
    [ "inGrowthpoint", "classRootImpedanceWhalley.html#ade7e1a35b3c27c6ada6b4ae975f4e51e", null ],
    [ "pSoilHydraulicHead", "classRootImpedanceWhalley.html#a5a73b7e2a084827bc99ccd76f83eb882", null ],
    [ "pSoilWaterContent", "classRootImpedanceWhalley.html#adb67023cf91970b22f211072f1bb9d77", null ],
    [ "wc_res", "classRootImpedanceWhalley.html#a16385325ff18ede975902fa4442fc8d6", null ],
    [ "wc_sat", "classRootImpedanceWhalley.html#a5db0166f555a179ed1e2f8f70d991060", null ]
];