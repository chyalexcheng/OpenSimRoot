var searchData=
[
  ['x_2276',['x',['../structCoordinate.html#a57422b67c5f69cd87c722bfcb6deb31b',1,'Coordinate::x()'],['../classMesh__base.html#a43b23fe5da87a08dfb4a8f897219b6ce',1,'Mesh_base::x()'],['../classOutputSoilVTK.html#a14f860198174b7851b891866db0268a3',1,'OutputSoilVTK::x()'],['../classSolute.html#aaeebac7fa0e511bb4df3ff10073eebd1',1,'Solute::x()'],['../classWatflow.html#aac0ccd4c755fa7bc48ad9533dbf198d9',1,'Watflow::x()']]],
  ['x_5fold_2277',['X_old',['../classBarberCushmanSolverOde23.html#ab2a9370c6b559e63244a2744a8ceeda7',1,'BarberCushmanSolverOde23::X_old()'],['../classBarberCushmanSolver.html#a6cf8d4999b1ebb1a1226ee796737202b',1,'BarberCushmanSolver::X_old()']]],
  ['xmax_2278',['xmax',['../classMesh__base.html#ac9bf4d6ea1d96583779deaed32194efc',1,'Mesh_base']]],
  ['xmin_2279',['xmin',['../classMesh__base.html#a783225adc4eaf689451b4e2f22ca71a4',1,'Mesh_base']]],
  ['xplist_2280',['xpList',['../classWaterUptakeDoussanModel.html#adc2e4f0f9e207bc1da68d60658c3a5eb',1,'WaterUptakeDoussanModel']]],
  ['xs_2281',['xs',['../classTillerFormation.html#a84baa61f5c015ab2a58772b254043b0e',1,'TillerFormation']]]
];
