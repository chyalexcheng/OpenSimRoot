var searchData=
[
  ['ja_911',['ja',['../structcsr__sparse.html#a40b3d7c7c036a4623d85b705958b4678',1,'csr_sparse']]],
  ['jacobi_912',['Jacobi',['../SolverTypes_8hpp.html#a840561b1786f56f574ba3c4e4ef06986ad265c8b1fda63fad2de4994858396ed8',1,'SolverTypes.hpp']]],
  ['jacobi_913',['JACOBI',['../SolverTypes_8hpp.html#a840561b1786f56f574ba3c4e4ef06986a0565cd5c5ac848bda19ad297b242ce5e',1,'SolverTypes.hpp']]],
  ['jmaxactivationenergy_914',['JmaxActivationEnergy',['../classLightLimitedPhotosynthesisRate.html#abf5c6f1b7cf8537693ee9a69b2a5ab36',1,'LightLimitedPhotosynthesisRate']]],
  ['jmaxdeactivationenergy_915',['JmaxDeactivationEnergy',['../classLightLimitedPhotosynthesisRate.html#aee155aaeebb2009191226a69e3a86c2d',1,'LightLimitedPhotosynthesisRate']]],
  ['jmaxentropyterm_916',['JmaxEntropyTerm',['../classLightLimitedPhotosynthesisRate.html#a5cb663b10905b3e5caf713002e6075ce',1,'LightLimitedPhotosynthesisRate']]],
  ['jmaxnitrogenproportionalityconstant_917',['JmaxNitrogenProportionalityConstant',['../classLightLimitedPhotosynthesisRate.html#a4cd92a9ff24b726dcb39effbc002ca5b',1,'LightLimitedPhotosynthesisRate']]],
  ['join_918',['join',['../structCoordinate.html#aacbd05a1c49ebddc7e263099937ac5c6',1,'Coordinate::join()'],['../structMovingCoordinate.html#a23484955cddafcd01322d70fbc7a44ea',1,'MovingCoordinate::join()']]]
];
