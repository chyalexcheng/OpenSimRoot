var classSimpleSoilTemperature =
[
    [ "SimpleSoilTemperature", "classSimpleSoilTemperature.html#ada9219879ef7a302b99da79c0fb202ea", null ],
    [ "calculate", "classSimpleSoilTemperature.html#ac8f81a1b72f6df0f1252b0e07132e4e8", null ],
    [ "getName", "classSimpleSoilTemperature.html#a77cb61514d807201266f36c4282a423d", null ],
    [ "hottest_time_of_the_day_", "classSimpleSoilTemperature.html#ae94ed715536a10d89608c8bf89061499", null ],
    [ "hottestDayOfYear_", "classSimpleSoilTemperature.html#a9ef4eb46d35b734bf6cb9e18b1f7f558", null ],
    [ "maxSurfaceTemperature_", "classSimpleSoilTemperature.html#a990b75a61a624d2ce0ddc26f6679a52f", null ],
    [ "minSurfaceTemperature_", "classSimpleSoilTemperature.html#afd3f2aaebcb18e304d208d2569820431", null ],
    [ "thermalConductivity_", "classSimpleSoilTemperature.html#a3ac67289ca9239ca0d32b7b25269b3d6", null ],
    [ "volumetricHeatCapacity_", "classSimpleSoilTemperature.html#afdd77817de314c89c3d3045ed78d80e5", null ],
    [ "yearlyMaxSurfaceTemperature_", "classSimpleSoilTemperature.html#ad0e9fafa57ee5c59063a9cdd136dceb1", null ],
    [ "yearlyMinSurfaceTemperature_", "classSimpleSoilTemperature.html#ac65fcb5d360a63168e97ea9fd99778aa", null ]
];