var classRootSegmentDryWeight =
[
    [ "RootSegmentDryWeight", "classRootSegmentDryWeight.html#a7e26fd4e74f6f26800e1c21e60f2171f", null ],
    [ "calculate", "classRootSegmentDryWeight.html#a6e58a861fb7d5d388fc751598eb39a6e", null ],
    [ "getName", "classRootSegmentDryWeight.html#a16ca2f8cc52907bb54caeca6d23383c3", null ],
    [ "postIntegrationCorrection", "classRootSegmentDryWeight.html#a6783fb95d8d83b2286be5a1a1e3b8c5f", null ],
    [ "densitySimulator", "classRootSegmentDryWeight.html#acccea2b072d8b484d35d00f66c5aae9e", null ],
    [ "dw", "classRootSegmentDryWeight.html#a24f60b76c7ac91c0c064989b257ceb01", null ],
    [ "mode", "classRootSegmentDryWeight.html#a5ffae7367e6725054d1a41e7cf7c63b1", null ],
    [ "volumeSimulator", "classRootSegmentDryWeight.html#acc35954a642a4ba22a087f918921c131", null ]
];