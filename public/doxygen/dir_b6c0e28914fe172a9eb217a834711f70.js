var dir_b6c0e28914fe172a9eb217a834711f70 =
[
    [ "GenerateRootNodes.cpp", "GenerateRootNodes_8cpp.html", null ],
    [ "GenerateRootNodes.hpp", "GenerateRootNodes_8hpp.html", [
      [ "RootDataPoints", "classRootDataPoints.html", "classRootDataPoints" ],
      [ "RootSegmentAge", "classRootSegmentAge.html", "classRootSegmentAge" ]
    ] ],
    [ "Geometry.cpp", "Geometry_8cpp.html", null ],
    [ "Geometry.hpp", "Geometry_8hpp.html", [
      [ "RootClassID", "classRootClassID.html", "classRootClassID" ],
      [ "RootCircumference", "classRootCircumference.html", "classRootCircumference" ],
      [ "RootLength2Base", "classRootLength2Base.html", "classRootLength2Base" ],
      [ "RootSegmentLength", "classRootSegmentLength.html", "classRootSegmentLength" ],
      [ "RootSegmentSurfaceArea", "classRootSegmentSurfaceArea.html", "classRootSegmentSurfaceArea" ],
      [ "RootSegmentRootHairSurfaceArea", "classRootSegmentRootHairSurfaceArea.html", "classRootSegmentRootHairSurfaceArea" ],
      [ "RootSegmentVolume", "classRootSegmentVolume.html", "classRootSegmentVolume" ],
      [ "RootSegmentVolumeCortex", "classRootSegmentVolumeCortex.html", "classRootSegmentVolumeCortex" ]
    ] ],
    [ "GrowthDirection.cpp", "GrowthDirection_8cpp.html", null ],
    [ "GrowthDirection.hpp", "GrowthDirection_8hpp.html", [
      [ "RootGrowthDirection", "classRootGrowthDirection.html", "classRootGrowthDirection" ],
      [ "RandomImpedence", "classRandomImpedence.html", "classRandomImpedence" ],
      [ "RandomGravitropism", "classRandomGravitropism.html", "classRandomGravitropism" ],
      [ "Tropisms", "classTropisms.html", "classTropisms" ]
    ] ],
    [ "GrowthImpedance.cpp", "GrowthImpedance_8cpp.html", "GrowthImpedance_8cpp" ],
    [ "GrowthImpedance.hpp", "GrowthImpedance_8hpp.html", [
      [ "RootImpedanceFromBulkDensity", "classRootImpedanceFromBulkDensity.html", "classRootImpedanceFromBulkDensity" ],
      [ "RootImpedanceGao", "classRootImpedanceGao.html", "classRootImpedanceGao" ],
      [ "RootImpedanceWhalley", "classRootImpedanceWhalley.html", "classRootImpedanceWhalley" ],
      [ "RootGrowthImpedanceRateMultiplier", "classRootGrowthImpedanceRateMultiplier.html", "classRootGrowthImpedanceRateMultiplier" ],
      [ "RootDiameterImpedance", "classRootDiameterImpedance.html", "classRootDiameterImpedance" ],
      [ "RootGrowthDirectionImpedance", "classRootGrowthDirectionImpedance.html", "classRootGrowthDirectionImpedance" ],
      [ "BioporeController", "classBioporeController.html", "classBioporeController" ]
    ] ],
    [ "LongitudinalGrowth.cpp", "LongitudinalGrowth_8cpp.html", null ],
    [ "LongitudinalGrowth.hpp", "LongitudinalGrowth_8hpp.html", [
      [ "ConstantRootGrowthRate", "classConstantRootGrowthRate.html", "classConstantRootGrowthRate" ],
      [ "ScaledRootGrowthRate", "classScaledRootGrowthRate.html", "classScaledRootGrowthRate" ]
    ] ],
    [ "RootBranching.cpp", "RootBranching_8cpp.html", null ],
    [ "RootBranching.hpp", "RootBranching_8hpp.html", [
      [ "RootBranches", "classRootBranches.html", "classRootBranches" ],
      [ "GenerateRoot", "classGenerateRoot.html", "classGenerateRoot" ],
      [ "InsertRootBranchContainers", "classInsertRootBranchContainers.html", "classInsertRootBranchContainers" ]
    ] ],
    [ "RootBranchingOfTillers.cpp", "RootBranchingOfTillers_8cpp.html", null ],
    [ "RootBranchingOfTillers.hpp", "RootBranchingOfTillers_8hpp.html", [
      [ "RootBranchesOfTillers", "classRootBranchesOfTillers.html", "classRootBranchesOfTillers" ]
    ] ],
    [ "RootDryWeight.cpp", "RootDryWeight_8cpp.html", "RootDryWeight_8cpp" ],
    [ "RootSegmentDryWeight.cpp", "RootSegmentDryWeight_8cpp.html", null ],
    [ "RootSegmentDryWeight.hpp", "RootSegmentDryWeight_8hpp.html", [
      [ "RootSegmentDryWeight", "classRootSegmentDryWeight.html", "classRootSegmentDryWeight" ],
      [ "RootSegmentSpecificWeight", "classRootSegmentSpecificWeight.html", "classRootSegmentSpecificWeight" ]
    ] ],
    [ "SecondaryGrowth.cpp", "SecondaryGrowth_8cpp.html", null ],
    [ "SecondaryGrowth.hpp", "SecondaryGrowth_8hpp.html", [
      [ "RootDiameter", "classRootDiameter.html", "classRootDiameter" ],
      [ "SumSteelCortex", "classSumSteelCortex.html", "classSumSteelCortex" ],
      [ "CortexDiameter", "classCortexDiameter.html", "classCortexDiameter" ],
      [ "SecondaryGrowth", "classSecondaryGrowth.html", "classSecondaryGrowth" ],
      [ "PotentialSecondaryGrowth", "classPotentialSecondaryGrowth.html", "classPotentialSecondaryGrowth" ]
    ] ]
];