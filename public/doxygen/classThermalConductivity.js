var classThermalConductivity =
[
    [ "ThermalConductivity", "classThermalConductivity.html#a4483f7229798935c3b32df6285022d4a", null ],
    [ "calculate", "classThermalConductivity.html#a137a904fb24f0dd7a23aff7d3d7138c6", null ],
    [ "getName", "classThermalConductivity.html#abf6f3cf3b10083d29c2f68b8d16a25a4", null ],
    [ "beta_t_", "classThermalConductivity.html#a2c2387dfa4d47f2442e6324155373d14", null ],
    [ "Q_", "classThermalConductivity.html#a84d7426a3043f797ad8a9fab08737f49", null ],
    [ "thermalConductivity_", "classThermalConductivity.html#a8fb80f4f1493d1021e05d768b3aeb319", null ],
    [ "theta_", "classThermalConductivity.html#a87f9bffc0ca4203b73f84f9fcad95516", null ],
    [ "theta_clay_", "classThermalConductivity.html#a805b75cfbfe088b1bb165abca64e3344", null ],
    [ "theta_other_minerals_", "classThermalConductivity.html#a53243c14e795c5e91bb50cf52b0f23e9", null ],
    [ "theta_quartz_", "classThermalConductivity.html#a4743b5838b996a9768a78261833f4eb6", null ]
];