var classBeamRadiationSimulator =
[
    [ "BeamRadiationSimulator", "classBeamRadiationSimulator.html#a43375b66c139200c1a61fede0f9c0e52", null ],
    [ "calculate", "classBeamRadiationSimulator.html#a012a0d8c046b71008b3eb40703f9dd93", null ],
    [ "getName", "classBeamRadiationSimulator.html#ab9a5d0fc675a29040802f2e8090539d7", null ],
    [ "cleanUpTime", "classBeamRadiationSimulator.html#a0a52e3303438d04a90367c904ba93ccc", null ],
    [ "cosLatitude", "classBeamRadiationSimulator.html#a57cf720a098886316a178de82c549484", null ],
    [ "cosSlope", "classBeamRadiationSimulator.html#af42308934f651797faa54683c4314edc", null ],
    [ "it1", "classBeamRadiationSimulator.html#aa626ce19fad9c6e54bd60a93d4c0d186", null ],
    [ "it2", "classBeamRadiationSimulator.html#ae34ac5fde5cffff3fc1931237d7f19db", null ],
    [ "newCleanUpTime", "classBeamRadiationSimulator.html#afab22bb66a88fed0584395c3f5fa7214", null ],
    [ "pAtmosphericPressure", "classBeamRadiationSimulator.html#a8f06e41c6f426601bed99af8a5537a57", null ],
    [ "pAtmosphericTransmissionCoefficient", "classBeamRadiationSimulator.html#a228f3853c7bf701673ff6ffd407b7220", null ],
    [ "pCloudCover", "classBeamRadiationSimulator.html#ad8af7cf9e0c5c0a7293f2a04bd2ed6ea", null ],
    [ "pReferenceSolarRadiation", "classBeamRadiationSimulator.html#aa564ae416ab5a28f99187b1d5809f513", null ],
    [ "savedValues", "classBeamRadiationSimulator.html#a9314770e76b45ec61f0490d495c68f7d", null ],
    [ "saz", "classBeamRadiationSimulator.html#a595dd0e98acd723a44eb28bae04eff19", null ],
    [ "sinLatitude", "classBeamRadiationSimulator.html#a720a2b6925a73af0f7334122650db760", null ],
    [ "sinSlope", "classBeamRadiationSimulator.html#a0a5a5333ed82ce651227101dc618f658", null ],
    [ "slope", "classBeamRadiationSimulator.html#ab482a50bb0225f7268cc5dff5f757b3b", null ],
    [ "solarElevationAngle", "classBeamRadiationSimulator.html#a684fadd46767ec1ab2b505fe76590069", null ],
    [ "startDay", "classBeamRadiationSimulator.html#ae2b97f1c5ba2b7e23a6418f65aa9a599", null ],
    [ "startYear", "classBeamRadiationSimulator.html#a5349219fad543ccdec0ce67f0df9c1f5", null ]
];