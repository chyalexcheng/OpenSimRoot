var classRootGrowthImpedanceRateMultiplier =
[
    [ "RootGrowthImpedanceRateMultiplier", "classRootGrowthImpedanceRateMultiplier.html#ac96e6fc0484af2bf658549db391c8ccc", null ],
    [ "calculate", "classRootGrowthImpedanceRateMultiplier.html#a05f6b16eaf14f0218d0cf8a8907bc768", null ],
    [ "getName", "classRootGrowthImpedanceRateMultiplier.html#a7cabc0d3c637524e03a1a73cc7dd4ff3", null ],
    [ "RootGrowthDirectionImpedance", "classRootGrowthImpedanceRateMultiplier.html#ab991013d208deb56438876fbd2ee8acd", null ],
    [ "bioPore", "classRootGrowthImpedanceRateMultiplier.html#a3648edb71bdf9bdcef8d6dcc8f564399", null ],
    [ "fastImpedanceCalculator", "classRootGrowthImpedanceRateMultiplier.html#aa115af24e36933d28aba540ba03ef8cb", null ],
    [ "halfGrowthImpedance", "classRootGrowthImpedanceRateMultiplier.html#a52b6dc9c3a026d941258b1ab4087e613", null ]
];