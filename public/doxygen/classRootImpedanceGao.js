var classRootImpedanceGao =
[
    [ "RootImpedanceGao", "classRootImpedanceGao.html#a7d6c7c31e9a492113a8183552eb69ada", null ],
    [ "calculate", "classRootImpedanceGao.html#a372b5a344a59126558610bf48174e57a", null ],
    [ "getName", "classRootImpedanceGao.html#a599aa2f46b8de475abe16b5c9c09bda0", null ],
    [ "bulkDensity", "classRootImpedanceGao.html#a5635c7eca81ff0c0be97287563d702d8", null ],
    [ "cum_stressPower", "classRootImpedanceGao.html#a754cdb39f68b2b50bf0160b71737b028", null ],
    [ "inGrowthpoint", "classRootImpedanceGao.html#ac76b9608d48ca438e97a5e5d2333f812", null ],
    [ "pSoilHydraulicHead", "classRootImpedanceGao.html#a16a1239aad4ad269fa27f69cf88759ee", null ],
    [ "pSoilWaterContent", "classRootImpedanceGao.html#ad9e3fe438a53e473809ccc0a59fa4cb0", null ],
    [ "vg_alpha", "classRootImpedanceGao.html#ae4d4dd10f3a2b15e2f942ebc69c36cc2", null ],
    [ "vg_n", "classRootImpedanceGao.html#aff14c21d6ee7e4ec6ba4f35a51b0ec61", null ],
    [ "void_ratio", "classRootImpedanceGao.html#a09a185f4422e80ca3b04ee874932382e", null ],
    [ "voidRatio", "classRootImpedanceGao.html#af399919d5ea591ee298a59c352081041", null ],
    [ "wc_res", "classRootImpedanceGao.html#a7b24621ba387fd5e7e3417428ebc80f1", null ],
    [ "wc_sat", "classRootImpedanceGao.html#ac6c6816fa42569c907a6d05607aec331", null ]
];