var classLeafSenescence =
[
    [ "LeafSenescence", "classLeafSenescence.html#ade06213c3da73e34505b65dc493d4dfd", null ],
    [ "calculate", "classLeafSenescence.html#a08b457af519b91a1cb0b63d147df0fb5", null ],
    [ "getName", "classLeafSenescence.html#a9513891e01fe13f12d1f16d9d546b477", null ],
    [ "CinDryWeight", "classLeafSenescence.html#a07ccb68e33e227e01f41630d027d5d33", null ],
    [ "leafArea", "classLeafSenescence.html#a7793d4ec37485fc684160b396f686fdf", null ],
    [ "lt", "classLeafSenescence.html#a6ab53ac9696f36acb1c02e001dbe5efc", null ],
    [ "plantingTime", "classLeafSenescence.html#afa099629233092563df9ecec7833a3c2", null ],
    [ "senescedLeafArea", "classLeafSenescence.html#a5a1098a9867ac271937894e404db2136", null ],
    [ "senescenceRate", "classLeafSenescence.html#adaa36bb826d08cd6598570c34c9aee15", null ],
    [ "SLA", "classLeafSenescence.html#ae7726a9d8d63491d11cd8107a9ef5142", null ]
];