var classRadiation =
[
    [ "Radiation", "classRadiation.html#ad59814ad87e1bf8da14d0c148f95e88c", null ],
    [ "calculate", "classRadiation.html#abde7efcfc84a49836fd775b7fd30c410", null ],
    [ "getName", "classRadiation.html#ada1ea16e36f4aaa36e6286fc0d32e045", null ],
    [ "actualDurationofSunshine_", "classRadiation.html#af7f1a6e9f4e3b23b61c3cd8d20f747c3", null ],
    [ "actualVaporPressure_", "classRadiation.html#aac4bee0dd8a0d74213f47ce3ae757fc7", null ],
    [ "albedo_", "classRadiation.html#a79c23398ebfa3d6a698fb90be2044b6a", null ],
    [ "angstrom_as_", "classRadiation.html#afb11d569c350dc4603cae0ff79cec9f4", null ],
    [ "angstrom_bs_", "classRadiation.html#afd540df4ee00337a1b4b54f1db7ff62f", null ],
    [ "dailyTemperature_", "classRadiation.html#a5e059bc9071da9a093f23d33e67d1ec4", null ],
    [ "latitude_", "classRadiation.html#acfd9f55698d8df608c6b744388d30a69", null ],
    [ "maxTemperature_", "classRadiation.html#af9aa3373a04867017584cef4639d059e", null ],
    [ "minTemperature_", "classRadiation.html#a31c9e5e7a11f3c4f901f2bad1e322323", null ],
    [ "netSolarRadiation_", "classRadiation.html#a3e5d5b37e1520ff78d9902ca3fc9e596", null ]
];