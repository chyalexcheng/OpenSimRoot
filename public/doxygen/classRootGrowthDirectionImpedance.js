var classRootGrowthDirectionImpedance =
[
    [ "RootGrowthDirectionImpedance", "classRootGrowthDirectionImpedance.html#a4e387db661c7d45b2158ee71d73f3781", null ],
    [ "calculate", "classRootGrowthDirectionImpedance.html#ad76a7095b94fc629e838e571dc62ff36", null ],
    [ "getName", "classRootGrowthDirectionImpedance.html#a5490167c7fba96a4e1fd8692363b178e", null ],
    [ "ImpedanceFactor", "classRootGrowthDirectionImpedance.html#a2be16bfcde737cf3134d9b13eb8f95c8", null ],
    [ "bioPore", "classRootGrowthDirectionImpedance.html#a75551712531261a240f150b3418793da", null ],
    [ "fastImpedanceCalculator", "classRootGrowthDirectionImpedance.html#a698f8fcb8f2afe59e67e59729041e641", null ],
    [ "growthPoint", "classRootGrowthDirectionImpedance.html#a2abc16f6546493663a31a1b6780153a9", null ],
    [ "maxBulkDensity", "classRootGrowthDirectionImpedance.html#a0550c2382f9b385136031d7b6bc01669", null ],
    [ "minBulkDensity", "classRootGrowthDirectionImpedance.html#a833a19a72b58bb40303ed90d4681871c", null ],
    [ "position", "classRootGrowthDirectionImpedance.html#af837c4f7611bbad17b80a58c3aa08867", null ]
];